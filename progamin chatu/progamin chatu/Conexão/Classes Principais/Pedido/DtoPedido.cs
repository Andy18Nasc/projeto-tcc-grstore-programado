﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoPedido
    {       
        public string cpf { get; set; }
        public string bairro { get; set; }
        public string rg { get; set; }
        public string cidade { get; set; }
        public string user { get; set; }
    }
}
