﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class BusinessCadastro
    {
        DatabaseCadastro db = new DatabaseCadastro();

        public int Salvar(DtoCadastro clint)
        {
            if (clint.nome == string.Empty)
            {
                throw new ArgumentException("nome é obrigatória.");
            }
            if (clint.email == string.Empty)
            {
                throw new ArgumentException("email é obrigatória.");
            }

            if (clint.idade == string.Empty)
            {
                throw new ArgumentException("idade é obrigatória.");
            }
            if (clint.senha == string.Empty)
            {
                throw new ArgumentException("senha é obrigatória.");
            }



            return db.Salvar(clint);
        }
    }
}
