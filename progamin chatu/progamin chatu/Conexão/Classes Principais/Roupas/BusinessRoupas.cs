﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class BusinessRoupas
    {
        DatabaseRoupas db = new DatabaseRoupas();      
        public int salvar(DtoRoupas manU)
        {
            DatabaseRoupas db = new DatabaseRoupas();
            return db.salvar(manU);
        }

        public void Alterar(DtoRoupas roupas)
        {
            if (roupas.cor == string.Empty)
            {
                throw new ArgumentException("Cor é obrigatória.");
            }
            if (roupas.roupa == string.Empty)
            {
                throw new ArgumentException("Roupa é obrigatória.");
            }
            if (roupas.categoria == string.Empty)
            {
                throw new ArgumentException("Categoria é obrigatória.");
            }
            if (roupas.numeracao == 0)
            {
                throw new ArgumentException("Numeracao é obrigatória.");
            }
            if (roupas.preco == 0)
            {
                throw new ArgumentException("Preço é obrigatória.");
            }

            if (roupas.genero == "empty")
            {
                throw new ArgumentException("O Genero é obrigatória.");
            }

            db.Alterar(roupas);

        }

        public List<DtoRoupas> consultar(string cor, string roupa, string categoria, int preco)
        {
            if (roupa == "todas")
            {
                roupa = string.Empty;
            }
            if (categoria == "todos")
            {
                categoria = string.Empty;
            }
            if (cor == "todos")
            {
                cor = string.Empty;
            }
            if (preco == 0)
            {
                cor = string.Empty;
            }
            return consultar(cor, roupa, categoria, preco);
        }


        public void remover(string cor, string roupa, string categoria, int preco)
        {
            db.remover(cor, roupa, categoria, preco);
        }

    }
}
