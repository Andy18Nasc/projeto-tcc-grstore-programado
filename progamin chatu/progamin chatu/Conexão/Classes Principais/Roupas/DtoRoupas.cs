﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    public class DtoRoupas
    {
        public int Id { get; set; }
        public string cor { get; set; }
        public string roupa { get; set; }
        public int numeracao { get; set; }
        public string categoria { get; set; }
        public int preco { get; set; }
        public string  genero { get; set; }
    }
}
