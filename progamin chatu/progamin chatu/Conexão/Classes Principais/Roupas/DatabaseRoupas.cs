﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseRoupas
    {
        public int salvar(DtoRoupas savler)
        {
            string script = @"Insert Into tb_roupas (nm_roupa, vl_numerecao, ds_categoria, nm_cor, vl_preco, ds_genero) Values (@nm_roupas, @vl_numeracao, @ds_categoria, @nm_cor, @vl_preco, @ds_genero)";
            List<MySqlParameter> roma = new List<MySqlParameter>();
            roma.Add(new MySqlParameter("nm_roupa", savler.roupa));
            roma.Add(new MySqlParameter("vl_numeracao", savler.numeracao));
            roma.Add(new MySqlParameter("ds_categoria", savler.categoria));
            roma.Add(new MySqlParameter("mn_cor", savler.cor));
            roma.Add(new MySqlParameter("vl_preco", savler.preco));
            roma.Add(new MySqlParameter("ds_genero", savler.genero));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, roma);
        }
        public void Alterar(DtoRoupas roupas)
        {
            string script =
            @"UPDATE tb_roupas 
                 SET nm_cor  = @nm_cor,
                     nm_roupa  = @nm_roupa,
	                 ds_categoria   = @ds_categoria,
	                 vl_numeracao   = @vl_numeracao,	                 
               WHERE id_roupa = @id_roupa";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cor", roupas.cor));
            parms.Add(new MySqlParameter("nm_roupa", roupas.roupa));
            parms.Add(new MySqlParameter("vl_numeracao", roupas.numeracao));
            parms.Add(new MySqlParameter("ds_categoria", roupas.categoria));
            ;
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void consultar(string cor, string roupa, string categoria, int preco)
        {
            string script =
            @"SELECT * 
                FROM tb_roupas 
               WHERE nm_nome like @nm_nome
               WHERE nm_cor like @nm_cor
               WHERE vl_preco Like @vl_preco
               AND ds_genero like @ds_genero  
                  ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cor", cor));
            parms.Add(new MySqlParameter("nm_nome", roupa));
            parms.Add(new MySqlParameter("ds_genero", categoria));
            parms.Add(new MySqlParameter("vl_preco", preco));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            
            List<DtoRoupas> roupas = new List<DtoRoupas>();
            
            while (reader.Read())
            {
                DtoRoupas newroupa = new DtoRoupas();
                newroupa.roupa = reader.GetString("nm_nome");
                newroupa.roupa = reader.GetString("nm_cor");
                newroupa.categoria = reader.GetString("ds_genero");
                roupas.Add(newroupa);
            }
            reader.Close();
            
            db.ExecuteSelectScript(script, parms);

        }
            public void remover(string cor, string roupa, string categoria, int preco)
            {
                string script =
                    @"Delete From tb_roupas Where nm_nome, Where vl_preco, Where ds_cor, and ds_genero = @nm_nome, @vl_preco, @ds_genero";
                List<MySqlParameter> parms = new List<MySqlParameter>();
                parms.Add(new MySqlParameter("nm_nome", roupa));
                parms.Add(new MySqlParameter("nm_cor", cor));
                parms.Add(new MySqlParameter("ds_categoria", categoria));
                parms.Add(new MySqlParameter("vl_preco", preco));
                Database db = new Database();
                db.ExecuteInsertScript(script, parms);
                
            }
        }
    }

