﻿using MySql.Data.MySqlClient;
using progamin_chatu.Conexão.Classes_Basicas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace progamin_chatu.Telinhas.Tabelas
{
    class DatabaseFuncionario
    {
        public DtoFuncionario Log(string nome, string cargo)
        {
            string script = @"Select * From tb_funcionario
                            WHERE nm_nome = @nm_nome
                            AND ds_cargo   = @ds_cargo";


            List<MySqlParameter> Lazio = new List<MySqlParameter>();
            Lazio.Add(new MySqlParameter("nm_nome", nome));
            Lazio.Add(new MySqlParameter("ds_cargo", cargo));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, Lazio);


            DtoFuncionario funcionario = null;
            //  E é aqui que a treta começa! :,( //
            if (reader.Read())
            {
                funcionario = new DtoFuncionario();
                funcionario.id = reader.GetInt32("id_funcionario");
                funcionario.nome = reader.GetString("nm_nome");
                funcionario.cargo = reader.GetString("ds_cargo");
                funcionario.permiadm = reader.GetBoolean("vf_perm_adm");
                funcionario.permipedido = reader.GetBoolean("vf_perm_pedido");
                funcionario.permiproduto = reader.GetBoolean("vf_perm_produto");
            }
            reader.Close();
            return funcionario;
        }

        public void Remover(int id)
        {
            string script =
                @"DELETE from TB_funcionario Where id_funcionario = @id_funcionario";

            List<MySqlParameter> parma = new List<MySqlParameter>();
            parma.Add(new MySqlParameter("id_funcionario", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parma);
        }

        public void alterar(DtoFuncionario sampdoria)
        {
            string script = @"Update Tb_GRstore Set nm_nome = @nm_nome,
                                                    ds_login = @ds_login,
                                                    ds_senha = @ds_senha,
                                   Where id_funcionario = @id_funcionario";

            List<MySqlParameter> fiorentina = new List<MySqlParameter>();
            fiorentina.Add(new MySqlParameter("nm_nome", sampdoria.nome));
            fiorentina.Add(new MySqlParameter("ds_cargo", sampdoria.cargo));
           

            Database db = new Database();
            db.ExecuteInsertScriptWithPk(script, fiorentina);
        }

        public List<DtoFuncionario> consultar(string nome, string cargo)
        {
            string script =
            @"SELECT * 
                FROM tb_funcionario 
               WHERE nm_nome like @nm_nome
               WHERE ds_login like @ds_login
               AND ds_senha like @ds_senha
                 ";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", nome));
            parms.Add(new MySqlParameter("ds_login", cargo));           


            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<DtoFuncionario> musicas = new List<DtoFuncionario>();

            while (reader.Read())
            {
                DtoFuncionario roupa = new DtoFuncionario();
                roupa.nome = reader.GetString("nm_nome");
                roupa.cargo = reader.GetString("ds_cargo");                
                musicas.Add(roupa);
            }
            reader.Close();

            return musicas;
        }
    }
}
