﻿using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
           
        }

        private void CadastrarProduto_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DtoRoupas ross = new DtoRoupas();
                ross.categoria  = txbcategoria.Text;                
                ross.preco = Convert.ToInt32(txtpreco.Text);
                ross.genero = cbxGenero.SelectedItem.ToString();


                BusinessRoupas buss = new BusinessRoupas();
                buss.salvar(ross);
                enviarmensagem("produto salvo no banco!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro ao Consultar: " + ex.Message);
            }
        }

        private void enviarmensagem(string message)
        {
            MessageBox.Show(message, "produto salvo", MessageBoxButtons.OK);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RemoverProdutu tela = new RemoverProdutu();
            tela.Show();
            Hide();

        }

        private void txtnumeracao_TextChanged(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void cbxGenero_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}

