﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class Administradora : Form
    {
        public Administradora()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ConsultarFuncionario tela = new ConsultarFuncionario();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Principal tela = new Principal();
            tela.Show();
            Hide();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Login tela = new Login();
            tela.Show();
            Hide();
        }

        private void Administradora_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
           cadastro_Funcionario tela = new cadastro_Funcionario();
            tela.Show();
            Hide();
        }
    }
}
