﻿using progamin_chatu.Conexão.Classes_Principais.Outras;
using progamin_chatu.Telinhas;
using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace progamin_chatu
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Cadastro tela = new Cadastro();
            tela.Show();
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string usuario = txtusuario.Text;
                string senha = txtsenha.Text;

                BusinessSistema business = new BusinessSistema();
                bool logou = business.Logar(usuario, senha);

                if (logou == true)
                {
                    Principal tela = new Principal();
                    tela.Show();
                    Hide();
                }
                else
                {
                    MessageBox.Show("User e Senha Invalidos!", "Girls Store", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message);
                
            }
           
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Administradora tela = new Administradora();
            tela.Show();
            Hide();

        }

        private void label7_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            // em breve, funcional (versao 1.2 final) // 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Principal tela = new Principal();
            tela.Show();
            tela.Hide();
        }
    }
}
