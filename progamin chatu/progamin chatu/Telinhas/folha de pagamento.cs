﻿using progamin_chatu.Conexão.Classes_Principais.Folha_de_Pagamento;
using progamin_chatu.Telinhas.Tabelas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace progamin_chatu.Telinhas
{
    public partial class folha_de_pagamento : Form
    {
        public folha_de_pagamento()
        {
            InitializeComponent();
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void folha_de_pagamento_Load(object sender, EventArgs e)
        {

        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {

        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label42_Click(object sender, EventArgs e)
        {

        }

        private void label43_Click(object sender, EventArgs e)
        {

        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            try
            {


                double SalarioBase = Convert.ToDouble(txtSalarioBase.Text);
                double PercentualHE = Convert.ToDouble(txtpercentualHE.Text);
                double QuantidadeHE = Convert.ToDouble(txtquntidadeHE.Text);
                double QuantidadeAtraso = Convert.ToDouble(txtquantAtraso.Text);
                double QuanTidadeFaltas = Convert.ToDouble(txtQuantDiasFaltas.Text);
                double QuantidadeDomingosFaltas = Convert.ToDouble(txtQuantDomingosFaltas.Text);
                double QuantdadeDeMenor = Convert.ToDouble(txtQuantSF.Text);
                double ValorConvenio = Convert.ToDouble(txtConvenio.Text);
                lblSubConvenio.Text = Convert.ToString(ValorConvenio);
                double ValorCestaBasica = Convert.ToDouble(txtCestaBasica.Text);
                lblSubCesta.Text = Convert.ToString(ValorCestaBasica);



                if (txtSalarioBase != null)
                {


                    Folha_Pagamento folha = new Folha_Pagamento();
                    double SH = folha.CalcularSH(SalarioBase);


                    double HE = folha.CalcularHE(SH, PercentualHE, QuantidadeHE);
                    lblHE.Text = Convert.ToString(HE);


                    double DSR = folha.CalcularDSRHE(HE, QuanTidadeFaltas, QuantidadeDomingosFaltas);
                    lbldsr.Text = Convert.ToString(DSR);


                    double Atraso = folha.CalcularAtrasos(SH, QuantidadeAtraso);
                    lblAtraso.Text = Convert.ToString(Atraso);



                    double Faltas = folha.CalcularFaltas(SalarioBase, QuanTidadeFaltas, QuantidadeDomingosFaltas);
                    lblSubFaltas.Text = Convert.ToString(Faltas);



                    double INSS = folha.CalcularINSS(SalarioBase, HE, DSR, Faltas, Atraso);
                    lblINSS.Text = Convert.ToString(INSS);



                    double BaseINSS = (SalarioBase + HE + DSR) - (Faltas + Atraso);
                    BaseINSS = Math.Round(BaseINSS, 2);
                    double fgts = folha.CalcularFGTS(BaseINSS);
                    lblFGTS.Text = Convert.ToString(fgts);



                    double IRRF = folha.CalcularIRRF(BaseINSS, INSS);
                    lblIRRF.Text = Convert.ToString(IRRF);



                    double VT = folha.CalcularVT(SalarioBase);
                    lblVT.Text = Convert.ToString(VT);



                    double SalarioFamilia = folha.CalcularSF(SalarioBase, QuantdadeDeMenor);
                    lblsubSF.Text = Convert.ToString(SalarioFamilia);



                    double VR = Convert.ToDouble(txtValorVR.Text);
                    lblSubVR.Text = VR.ToString();


                    double BonusSalarial = Convert.ToDouble(txtBonus.Text);
                    lblsubBonus.Text = BonusSalarial.ToString();


                    double total = folha.CalcularTotal(SalarioBase, HE, DSR, Faltas, Atraso, INSS, IRRF, VT, SalarioFamilia, BonusSalarial, ValorConvenio, ValorCestaBasica);
                    lblTotal.Text = total.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(" Todos os campos devem conter digitos presentes no conjunto dos numeros naturais, porém não devem ser nulos. Preencha os campos corretamente e tente outra vez." + ex.Message,
                    "Toc Toc Brasil",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                FolhaDTO dto = new FolhaDTO();

                dto.IdFuncionario = Convert.ToInt32(lblID.Text);
                dto.FGTS = Convert.ToDouble(lblFGTS.Text);
                dto.VT = Convert.ToDouble(lblVT.Text);
                dto.INSS = Convert.ToDouble(lblINSS.Text);
                dto.IRRF = Convert.ToDouble(lblIRRF.Text);
                dto.VR = Convert.ToDouble(lblSubVR.Text);
                dto.SalarioFamilia = Convert.ToDouble(lblsubSF.Text);
                dto.DSR = Convert.ToDouble(lbldsr.Text);
                dto.HE = Convert.ToDouble(lblHE.Text);
                dto.Atraso = Convert.ToDouble(lblAtraso.Text);
                dto.Falta = Convert.ToDouble(lblSubFaltas.Text);
                dto.Convenio = Convert.ToDouble(lblSubConvenio.Text);
                dto.CestaBasica = Convert.ToDouble(lblSubCesta.Text);
                dto.Bonus = Convert.ToDouble(lblsubBonus.Text);
                dto.SalarioLiquido = Convert.ToDouble(lblTotal.Text);

                dto.CPF = txtcpf.Text;


                FolhaBusiness ft = new FolhaBusiness();
                ft.salvar(dto);

                MessageBox.Show("Folha de Pagamento foi salva com sucesso! :)");

            }
            catch
            {
                MessageBox.Show("Ocorreu um erro! Verifique se todos os campos estão preenchidos corretamente ou entre em contato com o administrador. :(");

            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            DtoFuncionario dto = comboBox1.SelectedItem as DtoFuncionario;
            lblID.Text = Convert.ToString(dto.id);
            txtnome.Text = dto.nome;

        }
    }
}
